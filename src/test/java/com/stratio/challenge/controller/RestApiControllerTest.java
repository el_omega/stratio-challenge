package com.stratio.challenge.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.stratio.challenge.serviceImpl.DecryptServiceImpl;
import com.stratio.challenge.util.Constants;

/**
 * 
 * Test the controller in two cases, when the body of the POST is OK and when it
 * is malformed.
 * 
 * @author mbeteta
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(RestApiController.class)
public class RestApiControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private DecryptServiceImpl decryptService;

	/**
	 * Test if the JSON key is code. Because controller parse the word "code"
	 * for extract the UUID. In this case it has to return BAD_REQUEST.
	 * 
	 * @throws Exception
	 */
	@Test
	public void decodeOK() throws Exception {

		String sampleInput = "[\r\n\t{\"code\" : \"2952410b-0a94-446b-8bcb-448dc6e30b08\"},\r\n\t{\"code\" : \"6f9c15fa-ef51-4415-afab-36218d76c2d9\"},\r\n\t{\"code\" : \"2ab81c9b-1719-400c-a676-bdba976150eb\"}\r\n]";

		// Send course as body to /students/Student1/courses
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post(Constants.URI_API_PREFIX + Constants.URI_VERSION + Constants.URI_DECODE)
				.accept(MediaType.APPLICATION_JSON).content(sampleInput).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.OK.value(), response.getStatus());

	}

	/**
	 * Test if the JSON key is code. Because controller parse the word "code"
	 * for extract the UUID. In this case it has to return BAD_REQUEST.
	 * 
	 * @throws Exception
	 */
	@Test
	public void decodeKO() throws Exception {

		String sampleInput = "[\r\n\t{\"cAde\" : \"2952410b-0a94-446b-8bcb-448dc6e30b08\"},\r\n\t{\"code\" : \"6f9c15fa-ef51-4415-afab-36218d76c2d9\"},\r\n\t{\"code\" : \"2ab81c9b-1719-400c-a676-bdba976150eb\"}\r\n]";

		// Send course as body to /students/Student1/courses
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post(Constants.URI_API_PREFIX + Constants.URI_VERSION + Constants.URI_DECODE)
				.accept(MediaType.APPLICATION_JSON).content(sampleInput).contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());

	}

}
