package com.stratio.challenge.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.stratio.challenge.serviceImpl.DecryptServiceImpl;

/**
 * Test the Decrypt service.
 * 
 * @author mbeteta
 *
 */
@RunWith(SpringRunner.class)
public class DecryptServiceController {

	@TestConfiguration
	static class DecryptImplTestContextConfiguration {

		@Bean
		public DecryptService employeeService() {
			return new DecryptServiceImpl();
		}
	}

	@Autowired
	private DecryptServiceImpl decryptService;

	/**
	 * 
	 * Test if given a UUID return a correct decoded code.
	 * 
	 */
	@Test
	public void decodeOneCode() {

		String code = "6f9c15fa-ef51-4415-afab-36218d76c2d9";
		String decode = decryptService.decode(code);

		assertEquals("73-15-46-dc9876321", decode);
	}

}
