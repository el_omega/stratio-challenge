package com.stratio.challenge.application;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 
 * Test the context load and configurations.
 * 
 * @author mbeteta
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RestApiApplicationTest {

	@Test
	public void contextLoads() {
	}

}
