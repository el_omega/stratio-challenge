package com.stratio.challenge.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonValue;
import com.stratio.challenge.model.Response;
import com.stratio.challenge.serviceImpl.DecryptServiceImpl;
import com.stratio.challenge.util.Constants;


/** 
 * Controller of the API REST.
 * It only has the function to decode the UUID. 
 * @author mbeteta
 *
 */

@RestController
@RequestMapping(value = Constants.URI_API_PREFIX + Constants.URI_VERSION)
public class RestApiController {

	public static final Logger logger = LoggerFactory
			.getLogger(RestApiController.class);

	@Autowired
	DecryptServiceImpl decryptService;
	
	
	/**
	 * Function that given a UUIDs list in JSON format decodes them
	 * and returns the list with decoded UUIDs.
	 * @param  codes List of UUIDs in JSON format
	 * @return Decoded codes
	 */
	@PostMapping(Constants.URI_DECODE)
//	@RequestMapping(value = Constants.URI_DECODE, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> decode(@Valid @RequestBody String codes) {

		JsonValue valueJson = Json.parse(codes.toString());

		List<String> decodes = null;
		Response result = new Response();	
		if (valueJson.isArray()) {
			
					
			JsonArray array = valueJson.asArray();
			
			decodes = array
					.values()
					.stream()
					.map(code -> decryptService.decode(code.asObject().get("code").asString()))
					.collect(Collectors.toList());
		
			result.setResults(decodes);
			result.setStatus(HttpStatus.OK.toString());
		}
		
		return ResponseEntity.ok().body(result);
	}

}