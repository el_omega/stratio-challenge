package com.stratio.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.stratio.challenge" }) // same
																		// as
																		// @Configuration
																		// @EnableAutoConfiguration
																		// @ComponentScan
																		// combined
public class RestApiApp {

	public static void main(String[] args) {

		SpringApplication.run(RestApiApp.class, args);
	}

}
