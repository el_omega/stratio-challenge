package com.stratio.challenge.service;

import java.util.Arrays;
import java.util.Comparator;

/**
 * This is a functional interface that implements the methods for decode the
 * UUIDs
 * 
 * @author mbeteta
 *
 */

public interface DecryptService {

	/**
	 * Convert Hexadecimal to Decimal
	 * 
	 * @param s
	 *            A string of the hexadecimal number
	 * @return Decimal number
	 */
	static int hex2Decimal(String s) {
		return Integer.parseInt(s, 16);
	}

	/**
	 * Sum the HEX value of each char.
	 * 
	 * @param galaxy
	 *            First 8 digits of the UUID
	 * @return Sum in decimal
	 */
	static Integer galaxy(String galaxy) {

		return Arrays.stream(galaxy.split("")).map(x -> hex2Decimal(x)).reduce((x, y) -> x + y).get();

	}

	/**
	 * Max HEX value of the stream.
	 * 
	 * @param quadrant
	 *            4 digits of the UUID
	 * @return Max Hex value in decimal.
	 */

	static Integer quadrant(String quadrant) {

		return Arrays.stream(quadrant.split("")).map(x -> hex2Decimal(x)).max(Comparator.comparing(i -> i)).get();

	}

	/**
	 * Comparation of the third and fourth group of digits, get the MAX value
	 * for each position Sum the HEX value of each char of the previous
	 * comparation.
	 * 
	 * @param starsystem1
	 *            4 digits of the UUID
	 * @param starsystem2
	 *            4 digits of the UUID
	 * @return Sum in decimal
	 */
	static Integer starSystem(String starsystem1, String starsystem2) {

		String starsystem = starsystem1 + "-" + starsystem2;

		return Arrays.stream(starsystem.split("-")).map(starsys -> hex2Decimal(starsys))
				.max(Comparator.comparing(value -> value)).map(maxValue -> Integer.toHexString(maxValue).toString())
				.map(value -> galaxy(value)).get();
	}

	/**
	 * Order descending and if exists repeated chars, only get 1 of the repeated
	 * char.
	 * 
	 * @param planet
	 *            The last 12 digits of the UUID
	 * @return Hex value without repeated char.
	 */
	static String planet(String planet) {

		return Arrays.stream(planet.split("")).map(value -> hex2Decimal(value)).sorted(Comparator.comparing(i -> -i))
				.map(decimal -> Integer.toHexString(decimal).toString()).distinct()
				.reduce((value1, value2) -> value1 + value2).get();

	}
}
