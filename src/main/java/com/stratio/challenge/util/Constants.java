package com.stratio.challenge.util;

public class Constants {

	/**
	 * Prefix of REST API
	 */
	public static final String URI_API_PREFIX = "/api";

	public static final String URI_VERSION = "/v1";

	public static final String URI_DECODE = "/decode";

	private Constants() {
	}

}
