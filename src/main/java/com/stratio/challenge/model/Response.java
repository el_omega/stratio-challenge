package com.stratio.challenge.model;

import java.util.List;

/**
 * Model for create the Response of the API.
 * 
 * @author mbeteta
 *
 */

public class Response {

	String status;
	
	List<String> results;

	public Response() {
		super();
	}

	public Response(List<String> results, String status) {
		super();
		this.status = status;
		this.results = results;
	}

	public List<String> getResults() {
		return results;
	}

	public void setResults(List<String> results) {
		this.results = results;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}

}
