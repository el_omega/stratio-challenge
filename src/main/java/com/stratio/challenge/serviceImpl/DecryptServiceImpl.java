package com.stratio.challenge.serviceImpl;

import org.springframework.stereotype.Service;

import com.stratio.challenge.service.DecryptService;

/**
 * 
 * Class for apply all the methods of the interface DecryptService
 * 
 * @author mbeteta
 *
 */
@Service
public class DecryptServiceImpl implements DecryptService {

	/**
	 * It receives the UUID with all the digits and decodes it.
	 * 
	 * @param codes
	 *            A UUID like that 2952410b-0a94-446b-8bcb-448dc6e30b08
	 * @return Decoded value
	 */
	public String decode(String codes) {

		String[] code = codes.split("-");
		String result = DecryptService.galaxy(code[0]) + "-" + DecryptService.quadrant(code[1]) + "-"
				+ DecryptService.starSystem(code[2], code[3]) + "-" + DecryptService.planet(code[4]);

		return result;

	}

}
