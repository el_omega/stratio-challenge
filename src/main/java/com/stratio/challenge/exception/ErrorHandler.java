package com.stratio.challenge.exception;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Handler of the exception of the API REST.
 * 
 * @author mbeteta
 *
 */

@ControllerAdvice
public class ErrorHandler {

	/**
	 * Function that control that the key <K,V> of the JSON is equal to "code". 
	 * This is the key that the source code get for parse the code.
	 * 
	 * @param request HTTP Request send to the API
	 * @param ex Exception that occur in the source code. In this case NullPointerException
	 * @return Message with the info for the user in JSON format
	 */
	@ExceptionHandler(NullPointerException.class)
	public ResponseEntity<ErrorInfo> handleNullPointerException(
			HttpServletRequest request, NullPointerException ex) {
		StringBuilder errorMessage = new StringBuilder();
		errorMessage
				.append("There is a key malformed. Remember that key have to be code ");

		ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST.value(),
				errorMessage.toString(), request.getRequestURI());

		return new ResponseEntity<>(errorInfo, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Function that control that the value <K,V> of the JSON is contains a valid
	 * value for HEX 0123456789ABCDEF
	 * 
	 * @param request HTTP Request send to the API
	 * @param ex Exception that occur in the source code. In this case NumberFormatException
	 * @return Message with the info for the user in JSON format
	 */
	@ExceptionHandler(NumberFormatException.class)
	public ResponseEntity<ErrorInfo> handleNumberFormatException(
			HttpServletRequest request, NumberFormatException ex) {
		StringBuilder errorMessage = new StringBuilder();
		errorMessage
				.append("There is a value malformed. Remember that HEX is 0123456789ABCDEF");

		ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST.value(),
				errorMessage.toString(), request.getRequestURI());

		return new ResponseEntity<>(errorInfo, HttpStatus.BAD_REQUEST);
	}
}
